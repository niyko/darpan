<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMyclassroomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('my_classroom', function(Blueprint $table) {
            $table->string('type')->comment('0 => youtube, 1 => video, 2 => pdf, 3 => link')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('my_classroom', function(Blueprint $table) {
            $table->string('type')->comment('0 => document, 1 => video')->change();
        });
    }
}
