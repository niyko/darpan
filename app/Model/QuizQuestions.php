<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuizQuestions extends Model
{
    protected $softDelete = true;
    protected $table = 'quiz_questions';
    protected $fillable = ['exam_id', 'question'];

    public function options(){
        return $this->hasMany('App\Model\QuizOptions', 'question_id', 'id');
    }

    public function exam(){
        return $this->hasOne('App\Model\QuizExam', 'id', 'exam_id');
    }
}