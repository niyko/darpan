<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuizReports extends Model
{
    protected $softDelete = true;
    protected $table = 'quiz_reports';
    protected $fillable = ['class_id', 'user_id', 'option_id', 'exam_id'];
}