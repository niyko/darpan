<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MyClassroom extends Model
{
    protected $softDelete = true;
    protected $table = 'my_classroom';
    protected $fillable = ['title', 'user_id', 'description', 'url', 'type', 'class_id'];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
