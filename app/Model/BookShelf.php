<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BookShelf extends Model
{
    protected $softDelete = true;
    protected $table = 'book_shelf';
    protected $fillable = ['title', 'user_id', 'description', 'url', 'class_id'];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
