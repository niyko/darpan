<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Creata extends Model
{
    protected $softDelete = true;
    protected $table = 'creata';
    protected $fillable = ['title', 'user_id', 'description', 'url', 'class_id'];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
