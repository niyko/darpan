<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class ChatList extends Model
{
	protected $softDelete = true;
    protected $table = 'chat_list';
    protected $fillable = ['message', 'user_id', 'type'];
}
