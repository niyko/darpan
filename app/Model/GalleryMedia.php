<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GalleryMedia extends Model
{
    protected $softDelete = true;
    protected $table = 'gallery_media';
    protected $fillable = ['title', 'user_id', 'folder_id', 'url', 'type'];

    public function folder(){
        return $this->hasOne('App\Model\GalleryFolders', 'id', 'folder_id');
    }
}
