<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;

class QuizExam extends Model
{
    protected $softDelete = true;
    protected $table = 'quiz_exams';
    protected $fillable = ['user_id', 'class_id', 'title', 'description', 'total_questions', 'total_score'];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function classroom(){
        return $this->hasOne('App\Model\ClassList', 'id', 'class_id');
    }
}