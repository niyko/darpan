<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClassList extends Model
{
	protected $softDelete = true;
    protected $table = 'class_list';
}
