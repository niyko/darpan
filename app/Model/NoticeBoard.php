<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NoticeBoard extends Model
{
    protected $softDelete = true;
    protected $table = 'notice_board';
    protected $fillable = ['user_id', 'class_id', 'notice'];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function classroom(){
        return $this->hasOne('App\Model\ClassList', 'id', 'class_id');
    }
}