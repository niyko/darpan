<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuizOptions extends Model
{
    protected $softDelete = true;
    protected $table = 'quiz_options';
    protected $fillable = ['question_id', 'option', 'is_correct'];

    public function question(){
        return $this->hasOne('App\Model\QuizQuestions', 'id', 'question_id');
    }
}