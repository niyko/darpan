<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class SchoolInfo extends Model
{
	protected $softDelete = true;
    protected $table = 'school_info';
    protected $fillable = ['key', 'value'];
}
