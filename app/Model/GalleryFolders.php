<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GalleryFolders extends Model
{
    protected $softDelete = true;
    protected $table = 'gallery_folders';
    protected $fillable = ['title', 'user_id'];

    public function media(){
        return $this->hasMany('App\Model\GalleryMedia', 'folder_id', 'id');
    }
}
