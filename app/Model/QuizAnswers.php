<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuizAnswers extends Model
{
    protected $softDelete = true;
    protected $table = 'quiz_answers';
    protected $fillable = ['user_id', 'class_id', 'exam_id', 'score'];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}