<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ClassList;
use App\Model\ChatList;
use App\User;
use Auth;

class ChatController extends Controller
{
    public function show()
    {
    	$class_list = ClassList::all();
        return view('chat.show', compact('class_list'));
    }

    public function get(Request $request)
    {
        if(!empty($request->input('user_id'))){
            $chat_list = ChatList::where("user_id", $request->input('user_id'))->get()->toArray();
            $general = array(
                'chats' => $chat_list,
            );
        }
    	else {
            $chat_list = ChatList::where("user_id", Auth::id())->get()->toArray();
            $general = array(
                'chats' => $chat_list,
            );
        }
    	return response()->json(collect([$general,[
                'success' => true,
            ]])->collapse(), 200);
    }

    public function sent(Request $request, $type)
    {
        $message = $request->input('message');
        if(!empty($request->input('user_id'))){
            if($type==0){
                ChatList::create([
                    'message' => $message, 
                    'type' => 1,
                    'user_id' => $request->input('user_id'),
                ]);
            }
        }
        else {
            if($type==0){
                ChatList::create([
                    'message' => $message, 
                    'type' => 0,
                    'user_id' => Auth::id(),
                ]);
            }
        }
    }

    public function list()
    {
    	if(Auth::user()->is_admin){
            $users = User::where('class_id', Auth::user()->class_id)->whereNotIn('is_admin', [1])->get();
            return view('chat.list', compact('users'));
        }
    }

    public function admin($user_id)
    {
    	$user = User::where('id', $user_id)->first();
        return view('chat.show', compact('user'));
    }

    public function approve($user_id)
    {
    	User::where('id', $user_id)->update([
            'is_verified' => 1
        ]);
        return redirect()->back()->with('status', 'User successful approved');
    }

    public function uploadAttachment(Request $request){
        $file = $request->file('audio');
        $file = $request->audio;
        $new_file_name = hash('md5',microtime()).'.'.$file->extension();
        $request->audio->storeAs('chat', $new_file_name, 'uploads');
        return response()->json([
            "key" => $new_file_name
        ]);
    }
}
