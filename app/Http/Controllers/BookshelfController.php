<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\BookShelf;
use App\Model\ClassList;
use Auth;
use UploadedFile;

class BookshelfController extends Controller
{
    public function show(Request $request)
    {
        $classlist = ClassList::all();
        $sort_class = $request->input('sort');
        if($sort_class==null) $sort_class = Auth::user()->class_id;

        $class_name = ClassList::where('id', $sort_class)->first()->name;

        if(Auth::user()->is_super_admin){
            $files = BookShelf::where('class_id', $sort_class)->orderBy('id', 'desc')->paginate(12);
        }
        else if(Auth::user()->is_admin){
            $files = BookShelf::where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(12);
        }
        else {
            $files = BookShelf::where('class_id', Auth::user()->class_id)->orderBy('id', 'desc')->paginate(12);
        }
        return view('book_shelf.show', compact('files', 'class_name', 'classlist'));
    }

    public function create()
    {
        return view('book_shelf.create');
    }

    public function store(Request $request)
    {
        $title = $request->input('title');
        $description = $request->input('description');
        $file = $request->file('media');
        $file = $request->media;
        $new_file_name = hash('md5',microtime()).'.'.$file->extension();
        $request->media->storeAs('bookshelf', $new_file_name, 'uploads');

        BookShelf::create([
            'user_id' => Auth::id(),
            'title' => $title,
            'class_id' => Auth::user()->class_id,
            'description' => $description,
            'url' => $new_file_name,
            'type' => 1,
        ]);

        return redirect()->back()->with('status', 'Book is uploaded successful');
    }

    public function delete($id)
    {
        BookShelf::where('id', $id)->delete();
        return redirect()->back()->with('status', 'Book successful deleted');
    }
}
