<?php

namespace App\Http\Controllers;
use Auth;
use App\Model\SchoolInfo;
use Illuminate\Http\Request;

class MoreController extends Controller
{
    public function show()
    {
        $school_id = SchoolInfo::where('key', 'school_code')->first()->value;
        $school_notification_id = $school_id.'#S';
        $class_notification_id = $school_id.'#C'.Auth::user()->class_id;
        $user_notification_id = $school_id.'#U'.Auth::id();
        $type_notification_id = $school_id.'#T'.Auth::user()->is_admin;
        return view('more.show', compact('school_notification_id', 'class_notification_id', 'user_notification_id', 'type_notification_id', 'school_id'));
    }
}
