<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\GalleryFolders;
use App\Model\GalleryMedia;
use App\Model\ClassList;
use Auth;
use ImageOptimizer;
use UploadedFile;

class GalleryController extends Controller
{
    public function show()
    {
        $albums = GalleryFolders::orderBy('id', 'desc')->paginate(10);
        return view('gallery.show', compact('albums'));
    }

    public function album_create()
    {
        return view('gallery.album_create');
    }

    public function album_store(Request $request)
    {
        $title = $request->input('title');

        GalleryFolders::create([
            'user_id' => Auth::id(),
            'title' => $title
        ]);

        return redirect()->back()->with('status', 'Album added successfully');
    }

    public function album_delete($album_id)
    {
        GalleryFolders::where('id', $album_id)->delete();
        return redirect('gallery')->with('status', 'Album successful deleted');
    }

    public function gallery($album_id)
    {
        $pictures = GalleryMedia::where('folder_id', $album_id)->orderBy('id', 'desc')->paginate(10);
        $album = GalleryFolders::where('id', $album_id)->first();
        return view('gallery.gallery', compact('album', 'pictures'));
    }

    public function gallery_create($album_id)
    {
        $album = GalleryFolders::where('id', $album_id)->first();
        return view('gallery.upload', compact('album'));
    }

    public function gallery_store(Request $request, $album_id)
    {
        $title = $request->input('title');
        $file = $request->file('media');
        $file = $request->media;
        $new_file_name = hash('md5',microtime()).'.'.$file->extension();
        $request->media->storeAs('gallery', $new_file_name, 'uploads');

        if(!is_dir(public_path('uploads/gallery_thumbs'))){
            mkdir(public_path('uploads/gallery_thumbs'), 0777);
        }

        $valid_ext = array('png','jpeg','jpg');
        $location = public_path('uploads/gallery_thumbs/'.$new_file_name);
        $file_extension = pathinfo($location, PATHINFO_EXTENSION);
        $file_extension = strtolower($file_extension);
      
        if(in_array($file_extension,$valid_ext)){
            $this->compressImage(public_path('uploads/gallery/'.$new_file_name),$location,60);
        }else{
            ImageOptimizer::optimize(public_path('uploads/gallery/'.$new_file_name), public_path('uploads/gallery_thumbs/'.$new_file_name));
        }
      
        GalleryMedia::create([
            'user_id' => Auth::id(),
            'title' => $title,
            'folder_id' => $album_id,
            'url' => $new_file_name,
            'type' => 0,
        ]);

        return redirect()->back()->with('status', 'Media is uploaded successful');
    }

    public function compressImage($source, $destination, $quality) {
        $info = getimagesize($source);
        if ($info['mime'] == 'image/jpeg') 
            $image = imagecreatefromjpeg($source);
        elseif ($info['mime'] == 'image/gif') 
            $image = imagecreatefromgif($source);
        elseif ($info['mime'] == 'image/png') 
            $image = imagecreatefrompng($source);
        imagejpeg($image, $destination, $quality);
    }

}
