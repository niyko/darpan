<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\QuizExam;
use App\Model\QuizQuestions;
use App\Model\QuizOptions;
use App\Model\QuizAnswers;
use App\Model\QuizReports;
use App\Model\ClassList;
use Auth;
use DB;
use UploadedFile;

class QuizController extends Controller
{
    public function show(Request $request)
    {
        $classlist = ClassList::all();
        $sort_class = $request->input('sort');
        if($sort_class==null) $sort_class = Auth::user()->class_id;

        $class_name = ClassList::where('id', $sort_class)->first()->name;

        $answers = QuizAnswers::where('user_id', Auth::id())->get();
        if(Auth::user()->is_super_admin){
            $exams = QuizExam::where('class_id', $sort_class)->orderBy('id', 'desc')->paginate(12);
        }
        else if(Auth::user()->is_admin){
            $exams = QuizExam::where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(12);
        }
        else {
            $exams = QuizExam::where('class_id', Auth::user()->class_id)->where('is_published', 1)->orderBy('id', 'desc')->paginate(12);
        }
        return view('quiz.show', compact('exams', 'answers', 'classlist', 'class_name'));
    }

    public function create()
    {
        $classlist = ClassList::all();
        return view('quiz.create', compact('classlist'));
    }

    public function store(Request $request)
    {
        $title = $request->input('title');
        $description = $request->input('description');
        $class_id = $request->input('class_id');

        QuizExam::create([
            'user_id' => Auth::id(),
            'title' => $title,
            'class_id' => $class_id,
            'description' => $description,
            'total_questions' => 0,
            'total_score' => 0
        ]);

        return redirect()->back()->with('status', 'Exam is uploaded successfully');
    }

    public function delete($id)
    {
        QuizExam::where('id', $id)->delete();
        return redirect()->back()->with('status', 'Exam successfully deleted');
    }

    public function questions($exam_id)
    {
        $exam = QuizExam::where('id', $exam_id)->first();
        $questions = QuizQuestions::where('exam_id', $exam_id)->orderBy('id', 'desc')->paginate(12);
        return view('quiz.questions', compact('questions', 'exam'));
    }

    public function questions_create($exam_id)
    {
        $exam = QuizExam::where('id', $exam_id)->first();
        return view('quiz.create_question', compact('exam'));
    }

    public function questions_store(Request $request, $exam_id)
    {
        $question = $request->input('question');

        QuizExam::where('id', $exam_id)->update([
            "total_questions" => DB::raw('total_questions+1'),
            "total_score" => DB::raw('total_score+5')
        ]);

        QuizQuestions::create([
            'exam_id' => $exam_id,
            'question' => $question
        ]);

        return redirect()->back()->with('status', 'Question is added successfully');
    }

    public function questions_delete($question_id)
    {
        $question  = QuizQuestions::where('id', $question_id)->first();
        QuizExam::where('id', $question->exam_id)->update([
            "total_questions" => DB::raw('total_questions-1'),
            "total_score" => DB::raw('total_score-5')
        ]);
        QuizQuestions::where('id', $question_id)->delete();
        return redirect()->back()->with('status', 'Question successfully deleted');
    }

    public function options($question_id)
    {
        $question = QuizQuestions::where('id', $question_id)->first();
        $options = QuizOptions::where('question_id', $question_id)->orderBy('id', 'desc')->paginate(12);
        return view('quiz.options', compact('options', 'question'));
    }

    public function options_create($question_id)
    {
        $question = QuizQuestions::where('id', $question_id)->first();
        return view('quiz.create_option', compact('question'));
    }

    public function options_store(Request $request, $question_id)
    {
        $option = $request->input('option');
        $is_correct = ($request->input('is_correct')==null)?0:1;

        QuizOptions::create([
            'question_id' => $question_id,
            'option' => $option,
            'is_correct' => $is_correct,
        ]);

        return redirect()->back()->with('status', 'Option is added successfully');
    }

    public function options_delete($option_id)
    {
        QuizOptions::where('id', $option_id)->delete();
        return redirect()->back()->with('status', 'Option successfully deleted');
    }

    public function exam($exam_id)
    {
        $exam = QuizExam::where('id', $exam_id)->first();
        $questions = QuizQuestions::where('exam_id', $exam_id)->get();
        return view('quiz.exam', compact('questions', 'exam'));
    }

    public function exam_store(Request $request, $exam_id)
    {
        $score = 0;
        $answers = $request->input('answers');
        foreach($answers as $option_id => $answer){
            if(explode('::', $answer)[0]=="true") $score+=5;
            QuizReports::create([
                'user_id' => Auth::id(),
                'class_id' => Auth::user()->class_id,
                'exam_id' => $exam_id,
                'option_id' => explode('::', $answer)[1]
            ]);
        }

        QuizAnswers::create([
            'user_id' => Auth::id(),
            'class_id' => Auth::user()->class_id,
            'exam_id' => $exam_id,
            'score' => $score,
        ]);

        return redirect('quiz')->with('status', 'Exam saved successfully');
    }
    
    public function analyse($exam_id)
    {
        $exam = QuizExam::where('id', $exam_id)->first();
        $results = QuizAnswers::where('exam_id', $exam_id)->where('class_id', Auth::user()->class_id)->get();
        return view('quiz.analyse', compact('results', 'exam'));
    }

    public function view_answers($exam_id, $user_id)
    {
        $exam = QuizExam::where('id', $exam_id)->first();
        $questions = QuizQuestions::where('exam_id', $exam_id)->get();
        $answers = QuizReports::where('user_id', $user_id)->get();
        return view('quiz.view_answers', compact('questions', 'exam', 'answers'));
    }

    public function publish($exam_id)
    {
        QuizExam::where('id', $exam_id)->update([
            'is_published' => 1
        ]);
        return redirect()->back()->with('status', 'Published successfully');
    }
}
