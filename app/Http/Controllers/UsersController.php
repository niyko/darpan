<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ClassList;
use App\Model\ChatList;
use App\User;
use Auth;

class UsersController extends Controller
{
    public function show($class_id)
    {
        $classlist = ClassList::all();
        $class_name = ClassList::where('id', $class_id)->first()->name;
    	$users = User::where('class_id', $class_id)->orderBy('is_admin', 'desc')->get();
        return view('users.show', compact('users', 'classlist', 'class_name', 'class_id'));
    }

    public function changeType($user_id, $type)
    {
    	$users = User::where('id', $user_id)->update([
            'is_admin' => $type,
            'is_verified' => 1
        ]);
        
        return redirect()->back()->with('status', 'Type changed successfully');
    }

    public function verifiy($user_id)
    {
    	$users = User::where('id', $user_id)->update([
            'is_verified' => 1
        ]);
        
        return redirect()->back()->with('status', 'Verified successfully');
    }

    public function delete($user_id)
    {
    	$users = User::where('id', $user_id)->delete();
        return redirect()->back()->with('status', 'User has been deleted');
    }
}
