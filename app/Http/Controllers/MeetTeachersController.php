<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ClassList;
use App\Model\ChatList;
use App\User;
use Auth;

class MeetTeachersController extends Controller
{
    public function show()
    {
    	$teachers = User::where('is_admin', 1)->get();
        return view('meet_teachers.show', compact('teachers'));
    }
}
