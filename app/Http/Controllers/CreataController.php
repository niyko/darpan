<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Creata;
use App\Model\ClassList;
use App\Model\SchoolInfo;
use Illuminate\Support\Facades\Storage;
use Auth;
use OneSignal;

class CreataController extends Controller
{
    public function show(Request $request)
    {
        $classlist = ClassList::all();
        $sort_class = $request->input('sort');
        if($sort_class==null) $sort_class = Auth::user()->class_id;
        $class_name = ClassList::where('id', $sort_class)->first()->name;

        if(Auth::user()->is_super_admin){
            $files = Creata::where('class_id', $sort_class)->orderBy('id', 'desc')->paginate(12);
        }
        else if(Auth::user()->is_admin){
            $files = Creata::where('class_id', Auth::user()->class_id)->orderBy('id', 'desc')->paginate(12);
        }
        else {
            $files = Creata::where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(12);
        }
        return view('creata.show', compact('files', 'class_name', 'classlist'));
    }

    public function create()
    {
        return view('creata.create');
    }

    public function store(Request $request)
    {
        $school_id = SchoolInfo::where('key', 'school_code')->first()->value;
        $title = $request->input('title');
        $link = $request->input('link');
        $description = $request->input('description');
        $type = $request->input('type');

        if($type==2){
            $new_file_name = $link;
        }
        else {
            $extension = ($type==1)?".mp4":".png";
            $file = $request->file('upload_file');
            $file = $request->upload_file;
            $new_file_name = hash('md5',microtime()).$extension;
            $request->upload_file->storeAs('creata', $new_file_name, 'uploads');
        }

        // if($image!=""){
        //     list($type, $image) = explode(';', $image);
        //     list(, $image)      = explode(',', $image);
        //     $image = base64_decode($image);

        //     Storage::disk('uploads')->put('creata/'.$new_file_name, $image);
        // }
        // else{
        //     $new_file_name = $link;
        // }

        Creata::create([
            'user_id' => Auth::id(),
            'title' => $title,
            'class_id' => Auth::user()->class_id,
            'description' => $description,
            'url' => $new_file_name,
            'type' => 1,
        ]);

        // $notification_rule = array(
        //     ["field"=> "tag", "key" => "class_id", "relation" => "=", "value" => $school_id."#U".Auth::user()->class_id],
        //     ["operator" => "AND"],
        //     ["field"=> "tag", "key" => "type_id", "relation" => "=", "value" => $school_id."#T1"]
        // );

        // $parameters = [
        //     'headings' => [
        //         'en' => 'New Creata file uploded'
        //     ],
        //     'contents' => [
        //         'en' => $title
        //     ],
        //     'priority' => 10,
        //     'filters' => $notification_rule
        // ];

        // OneSignal::sendNotificationCustom($parameters);

        return redirect()->back()->with('status', 'Video is uploaded successful');
    }

    public function delete($id)
    {
        Creata::where('id', $id)->delete();
        return redirect()->back()->with('status', 'Video successful deleted');
    }

    public function compressImage($source, $destination, $quality) {
        $info = getimagesize($source);
        if ($info['mime'] == 'image/jpeg') 
            $image = imagecreatefromjpeg($source);
        elseif ($info['mime'] == 'image/gif') 
            $image = imagecreatefromgif($source);
        elseif ($info['mime'] == 'image/png') 
            $image = imagecreatefrompng($source);
        imagejpeg($image, $destination, $quality);
    }

}
