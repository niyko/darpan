<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\MyClassroom;
use App\Model\SchoolInfo;
use App\Model\ClassList;
use Auth;
use UploadedFile;
use OneSignal;

class MyClassroomController extends Controller
{
    public function show(Request $request)
    {
        $classlist = ClassList::all();
        $sort_class = $request->input('sort');
        if($sort_class==null) $sort_class = Auth::user()->class_id;

        $class_name = ClassList::where('id', $sort_class)->first()->name;

        if(Auth::user()->is_super_admin){
            $files = MyClassroom::where('class_id', $sort_class)->orderBy('id', 'desc')->paginate(12);
        }
        else if(Auth::user()->is_admin){
            $files = MyClassroom::where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(12);
        }
        else {
            $files = MyClassroom::where('class_id', Auth::user()->class_id)->orderBy('id', 'desc')->paginate(12);
        }
        return view('my_classroom.show', compact('files', 'class_name', 'classlist'));
    }

    public function create()
    {
        $classlist = ClassList::all();
        return view('my_classroom.create', compact('classlist'));
    }

    public function store(Request $request)
    {
        $school_id = SchoolInfo::where('key', 'school_code')->first()->value;
        $title = $request->input('title');
        $description = $request->input('description');
        $type = $request->input('type');
        $class_id = $request->input('class_id');

        if($type==0){
            $url = $request->input('upload-youtube');
        }
        else if($type==1){
            $file = $request->file('upload_video');
            $file = $request->upload_video;
            $url = hash('md5',microtime()).'.mp4';
            $request->upload_video->storeAs('myclassroom', $url, 'uploads');
        }
        else if($type==2){
            $file = $request->file('upload_pdf');
            $file = $request->upload_pdf;
            $url = hash('md5',microtime()).'.pdf';
            $request->upload_pdf->storeAs('myclassroom', $url, 'uploads');
        }
        else {
            $url = $request->input('upload-link');
        }

        MyClassroom::create([
            'user_id' => Auth::id(),
            'title' => $title,
            'class_id' => $class_id,
            'description' => $description,
            'url' => $url,
            'type' => $type
        ]);

        // $notification_rule = array(
        //     ["field"=> "tag", "key" => "class_id", "relation" => "=", "value" => $school_id."#U".$class_id],
        //     ["operator" => "AND"],
        //     ["field"=> "tag", "key" => "type_id", "relation" => "=", "value" => $school_id."#T0"]
        // );

        // $parameters = [
        //     'headings' => [
        //         'en' => 'New Myclassroom chapter published'
        //     ],
        //     'contents' => [
        //         'en' => $title
        //     ],
        //     'priority' => 10,
        //     'filters' => $notification_rule
        // ];
    
        // OneSignal::sendNotificationCustom($parameters);

        return redirect()->back()->with('status', 'Media is uploaded successfully');
    }

    public function delete($id)
    {
        MyClassroom::where('id', $id)->delete();
        return redirect()->back()->with('status', 'Media successfully deleted');
    }
}
