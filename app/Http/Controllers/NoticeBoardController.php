<?php

namespace App\Http\Controllers;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use Illuminate\Http\Request;
use App\Model\MyClassroom;
use App\Model\ClassList;
use App\Model\NoticeBoard;
use App\Model\SchoolInfo;
use Auth;
use OneSignal;

class NoticeBoardController extends Controller
{
    public function show()
    {
        if(Auth::user()->is_super_admin)  $noticebaord = NoticeBoard::orderBy('id', 'desc')->paginate(10);
        else $noticebaord = NoticeBoard::whereIn('class_id', [-1,Auth::user()->class_id])->orderBy('id', 'desc')->paginate(10);
        return view('notice_board.show', compact('noticebaord'));
    }

    public function create()
    {
        $classlist = ClassList::all();
        return view('notice_board.create', compact('classlist'));
    }

    public function store(Request $request)
    {
        $school_id = SchoolInfo::where('key', 'school_code')->first()->value;
        $notice = $request->input('notice');
        $class_id = $request->input('class_id');

        $notice = preg_replace('"\b(https?://\S+)"', '<a href="$1?open_in_url">$1</a>', $notice);

        NoticeBoard::create([
            'user_id' => Auth::id(),
            'notice' => $notice,
            'class_id' => $class_id,
        ]);

        if($class_id==-1) $notification_rule = array(
            ["field"=>"tag", "key" => "school_id", "relation" => "=", "value" => $school_id."#S"],
            ["operator" => "AND"],
            ["field"=>"tag", "key" => "type_id", "relation" => "=", "value" => $school_id."#T0"]
        );
        else $notification_rule = array(
            ["field"=> "tag", "key" => "class_id", "relation" => "=", "value" => $school_id."#U".$class_id],
            ["operator" => "AND"],
            ["field"=> "tag", "key" => "type_id", "relation" => "=", "value" => $school_id."#T0"]
        );

        $parameters = [
            'headings' => [
                'en' => 'New notice published'
            ],
            'contents' => [
                'en' => $notice
            ],
            'priority' => 10,
            'filters' => $notification_rule
        ];
    
        OneSignal::sendNotificationCustom($parameters);
        
        return redirect()->back()->with('status', 'Notice has been sent');
    }

    public function delete($id)
    {
        NoticeBoard::where('id', $id)->delete();
        return redirect()->back()->with('status', 'Notice deleted successfully');
    }

}
