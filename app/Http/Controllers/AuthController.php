<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ClassList;
use App\Model\ChatList;
use App\User;
use Auth;

class AuthController extends Controller
{
    public function logout(){
        Auth::logout();
        return redirect('/');
    }

    public function openApp(Request $request)
    {
        $maintaince_mode = false;

        if($maintaince_mode) return redirect('/maintaince');
        else {
            $app_version = $request->input('version');
            $current_version = 4;
            if($app_version < $current_version){
                return view('app.update');
            }
            else {
                return redirect('/');
            }
        }
    }
}
