<!DOCTYPE html>
<html>
    <head>
        <title>Darpan</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.4.6/dist/css/uikit.min.css" />
        <link rel="stylesheet" href="{{asset('css/main.css')}}?integrity={{integrity('css/main.css')}}" />
        <link rel="stylesheet" href="{{asset('css/icons.css')}}" />
        @stack('head')
    </head>
    <body>
        @yield('body')
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.4.6/dist/js/uikit.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src='{{asset('js/hrefforeverything.js')}}'></script>
        @stack('script')
    </body>
</html>