@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/my_classroom.css')}}?integrity={{integrity('css/my_classroom.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('more-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">My classroom</p>
            </div>
            @if (Auth::user()->is_admin)
                <div class="uk-width-auto">
                    <button href="{{route('myclassroom-create')}}" class="nav_sub_icon_btn"><i class="icon-upload"></i></button>
                </div>
            @endif

            @if (Auth::user()->is_super_admin)
                <div class="uk-width-auto">
                    <button href="#modal-example" uk-toggle class="nav_sub_icon_btn uk-width-auto"><i class="icon-filter"></i> <span class="nav_sub_icon_btn_chip">{{$class_name}}</span></button>
                    
                    <div id="modal-example" uk-modal>
                        <div class="uk-modal-dialog myclassroom-drop">
                            <ul class="uk-list uk-list-divider uk-padding-remove">
                                @foreach ($classlist as $class)
                                    <li><a class="myclassroom-drop-link uk-margin-medium-right" href="{{route('myclassroom-show')}}?sort={{$class->id}}">{{$class->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')

        <div uk-grid="masonry: true">
            @foreach ($files as $file)
                @if ($file->user!=null)
                    <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
                        <div class="file-card uk-width-1-1 uk-box-shadow-large">
                            @if ($file->type == 0)
                                @php
                                $url = $file->url;
                                if (stristr($url,'youtu.be/')){
                                    preg_match('/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID);
                                    try {
                                        $ytid = $final_ID[4]; 
                                    } catch (\Exception $e) {
                                        $ytid = '634987489755';
                                    }
                                }
                                else {
                                    preg_match('/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD);
                                    try {
                                        $ytid = $IDD[5];
                                    } catch (\Exception $e) {
                                        $ytid = '634987489755';
                                    }
                                }
                                @endphp
                                <img class="file-card-youtube-image" src="https://img.youtube.com/vi/{{$ytid}}/hqdefault.jpg">
                            @elseif ($file->type == 1)
                                <img class="file-card-cover" src="{{asset('images/video_placeholder.png')}}">
                            @elseif ($file->type == 2)
                                <img class="file-card-cover" src="{{asset('images/doc_placeholder.png')}}">
                            @endif
    
                            @if ($file->type != 3)
                                <span class="uk-badge file-card-time">{{\Carbon\Carbon::parse($file->created_at)->format('d M Y')}}</span>
                            @endif
                            
                            <div uk-grid class="uk-grid-collapse">
                                @if ($file->type == 3)
                                <div class="uk-width-auto" style="padding-bottom: 20px;">
                                    <button class="file-card-link-icon" style="background-image: url('{{asset('images/link_placeholder.png')}}')"><i class="icon-link"></i></button>
                                </div>
                                @endif
                                <div class="uk-width-expand">
                                    <div class="uk-padding-small">
                                        @if ($file->type != 3)
                                            <p class="file-card-title file-card-time-title">{{$file->title}}</p>
                                        @else
                                            <p class="file-card-title">{{$file->title}}</p>
                                        @endif
    
                                        <p class="file-card-description">{{$file->description}}</p>
    
                                        @if (Auth::user()->is_super_admin)
                                            <p class="file-card-username"><i class="icon-user"></i> {{(($file->user_id==Auth::id())?'You':$file->user->name)}}</p>
                                        @endif
    
                                        @if ($file->type == 0)
                                            <a class="file-card-link" href="{{$file->url}}?&meet.google&open_in_youtube&video_id={{urlencode($ytid)}}"><i class="icon-eye"></i> View</a>
                                        @elseif ($file->type == 1)
                                            <a class="file-card-link" href="{{asset('uploads/myclassroom/'.$file->url)}}?&meet.google&open_in_video&title={{urlencode($file->title)}}"><i class="icon-eye"></i> View</a>
                                        @elseif ($file->type == 2)
                                            <a class="file-card-link" href="{{asset('uploads/myclassroom/'.$file->url)}}?&meet.google&open_in_pdf&title={{urlencode($file->title)}}"><i class="icon-eye"></i> View</a>
                                        @elseif ($file->type == 3)
                                            <a class="file-card-link" href="{{$file->url}}?&meet.google&open_in_url&title={{urlencode($file->title)}}"><i class="icon-eye"></i> View</a>
                                        @endif
    
                                        @if ($file->user_id==Auth::id() || Auth::user()->is_super_admin)
                                            <a class="file-card-link" href="{{route('myclassroom-delete', ['id' => $file->id])}}"><i class="icon-trash-2"></i> Delete</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        {{ $files->links() }}
    </div>

    @unless (count($files))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Files are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush