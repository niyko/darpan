@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/my_classroom.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('myclassroom-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Upload</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')

        <form action="{{route('myclassroom-store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <label>Upload title</label>
            <input name="title" required maxlength="50" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">

            <label>Upload description</label>
            <input name="description" required maxlength="200" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">

            <label>Class</label>
            <div class="form-select">
                <select name="class_id" required class="uk-select">
                    <option selected disabled>Select one</option>
                    @foreach ($classlist as $class)
                        <option value="{{$class->id}}">{{$class->name}}</option>
                    @endforeach
                </select>
            </div>

            <hr class="uk-width-1-1">

            <input autocomplete="off" type="hidden" name="type" value="0">

            <div uk-grid class="uk-grid-collapse uk-margin-small-top uk-margin-small-bottom">
                <div class="uk-width-auto">
                    <button type="button" onclick="changeUploadSlide(0,this)" class="toggle-button active">youtube</button>
                </div>
                <div class="uk-width-auto">
                    <button type="button" onclick="changeUploadSlide(1,this)" class="toggle-button">video</button>
                </div>
                <div class="uk-width-auto">
                    <button type="button" onclick="changeUploadSlide(2,this)" class="toggle-button">pdf</button>
                </div>
                <div class="uk-width-auto">
                    <button type="button" onclick="changeUploadSlide(3,this)" class="toggle-button">link</button>
                </div>
            </div>

            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow id="myclassroom-upload-slider">
                <ul class="uk-slideshow-items">
                    <li>
                        <label>Upload youtube link</label>
                        <input autocomplete="off" name="upload-youtube" maxlength="300" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">
                    </li>
                    <li>
                        <label>Upload video</label>
                        <div uk-form-custom="target: true" class="form-input uk-width-1-1">
                            <input autocomplete="off" name="upload_video" type="file" accept="video/*">
                            <input class="uk-width-1-1 uk-form-width-medium form-upload" type="text" placeholder="Select file" disabled>
                        </div>
                    </li>
                    <li>
                        <label>Upload pdf</label>
                        <div uk-form-custom="target: true" class="form-input uk-width-1-1">
                            <input autocomplete="off" name="upload_pdf" type="file" accept="application/pdf">
                            <input class="uk-width-1-1 uk-form-width-medium form-upload" type="text" placeholder="Select file" disabled>
                        </div>
                    </li>
                    <li>
                        <label>Upload link</label>
                        <input autocomplete="off" name="upload-link" maxlength="300" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">
                    </li>
                </ul>
            </div>

            <button class="uk-width-1-1 btn-primary" type="submit"><i class="icon-upload"></i> Upload</button>
        </form>
    </div>
    
@endsection

@push('script')
<script src="{{asset('js/jquery.form.js')}}"></script> 
<script type="text/javascript">

    function changeUploadSlide(index,e){
        $('[name=type]').val(index);
        $('.toggle-button').removeClass('active');
        $(e).addClass('active');
        UIkit.slideshow('#myclassroom-upload-slider').show(index);
    }

    $('form').ajaxForm({
        beforeSend: function() {
            console.log(`--show-loading`);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            console.log(`--set-loading::${percentComplete}`);
        },
        complete: function(xhr) {
            window.location = `{{route('myclassroom-show')}}`;
        }
    });
</script>
@endpush