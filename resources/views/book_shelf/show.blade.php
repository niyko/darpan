@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/book_shelf.css')}}?integrity={{integrity('css/book_shelf.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('more-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Book shelf</p>
            </div>
            @if (Auth::user()->is_admin)
                <div class="uk-width-auto">
                    <button href="{{route('bookshelf-create')}}" class="nav_sub_icon_btn"><i class="icon-upload"></i></button>
                </div>
            @endif
            @if (Auth::user()->is_super_admin)
                <div class="uk-width-auto">
                    <button href="#modal-example" uk-toggle class="nav_sub_icon_btn uk-width-auto"><i class="icon-filter"></i> <span class="nav_sub_icon_btn_chip">{{$class_name}}</span></button>
                    
                    <div id="modal-example" uk-modal>
                        <div class="uk-modal-dialog bookshelf-drop">
                            <ul class="uk-list uk-list-divider uk-padding-remove">
                                @foreach ($classlist as $class)
                                    <li><a class="bookshelf-drop-link uk-margin-medium-right" href="{{route('bookshelf-show')}}?sort={{$class->id}}">{{$class->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
    
    <div class="uk-padding-small creata-list-padding-bottom-large">
        @include('components.status_alert')

        <div uk-grid="masonry: true">
            @foreach ($files as $file)
                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
                    <div class="file-card uk-width-1-1 uk-box-shadow-large">
                        <img class="uk-width-1-1" src="{{asset('images/doc_placeholder.png')}}">
                        <div class="uk-padding-small">
                            <p class="file-card-title">{{$file->title}}</p>
                            <p class="file-card-description">{{$file->description}}</p>
                            <a class="file-card-link" href="{{asset('uploads/bookshelf/'.$file->url)}}?open_in_pdf"><i class="icon-eye"></i> View</a>
                            @if ($file->user_id==Auth::id())
                                <a class="file-card-link" href="{{route('bookshelf-delete', ['id' => $file->id])}}"><i class="icon-trash-2"></i> Delete</a>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        {{ $files->links() }}

    </div>

    @unless (count($files))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Books are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush