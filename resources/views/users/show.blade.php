@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/user.css')}}?integrity={{integrity('css/user.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('more-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Users</p>
            </div>

            @if (Auth::user()->is_super_admin)
                <div class="uk-width-auto">
                    <button href="#modal-sort" uk-toggle class="nav_sub_icon_btn uk-width-auto"><i class="icon-filter"></i> <span class="nav_sub_icon_btn_chip">{{$class_name}}</span></button>
                    
                    <div id="modal-sort" uk-modal>
                        <div class="uk-modal-dialog users-drop">
                            <ul class="uk-list uk-list-divider uk-padding-remove">
                                @foreach ($classlist as $class)
                                    <li><a class="users-drop-link uk-margin-medium-right" href="{{route('users-show', ['class_id' => $class->id])}}">{{$class->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
    
    <div class="uk-padding-small" id="users">
        @include('components.status_alert')

        <input type="text" class="search list-search-box uk-width-1-1" placeholder="Search">

        <ul class="uk-list uk-list-divider list">
             @foreach ($users as $user)
                @if ($user->id!=Auth::id())
                <li>
                    <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
                        <div class="uk-width-auto">
                            <img class="users-list-pic" src="{{(($user->is_admin)?asset('images/teacher_placeholder.png'):asset('images/user_placeholder.png'))}}">
                        </div>
                        <div class="uk-width-expand name">
                            {{$user->name}}
                            @if (!$user->is_verified)
                                <span class="uk-label list-chip">Unverifed</span>
                            @endif
                        </div>
                        <div class="uk-width-auto">
                            <button class="btn-primary btn-small">MORE</button>
                            <div uk-dropdown="mode: click; pos: left-bottom" class="user-drop">
                                <ul class="uk-list uk-list-divider uk-padding-remove">
                                    @if (!$user->is_admin)
                                        <li><a class="user-drop-link" href="{{route('users-changetype', ['user_id' => $user->id, 'type' => 1])}}">Make as taecher</a></li>
                                    @else
                                        <li><a class="user-drop-link" href="{{route('users-changetype', ['user_id' => $user->id, 'type' => 0])}}">Make as parent</a></li>
                                    @endif

                                    @if (!$user->is_verified)
                                        <li><a class="user-drop-link" href="{{route('users-verifiy', ['user_id' => $user->id])}}">Verify</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="uk-width-auto">
                            <button class="btn-primary btn-small" href="{{route('users-delete', ['user_id' => $user->id])}}"><i class="icon-trash-2"></i></button>
                        </div>
                    </div>
                </li>
                @endif
            @endforeach
        </ul>
    </div>

    @empty($users->toArray())
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Users are empty, nothing to show</p>
            </div>
        </div>
    @endempty

@endsection

@push('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
<script type="text/javascript">
	var options = {
        valueNames: [ 'name' ]
    };

    var userList = new List('users', options);
</script>
@endpush