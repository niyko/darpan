@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/chat.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('more-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">My Teachers</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')

        <ul class="uk-list uk-list-divider">
             @foreach ($teachers as $teacher)
                <li>
                    <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
                        <div class="uk-width-auto">
                            <img class="chat-user-pic" src="{{asset('images/user_placeholder.png')}}">
                        </div>
                        <div class="uk-width-expand">
                            {{$teacher->name}}
                        </div>
                        <div class="uk-width-auto">
                            <button class="list-btn" href="tel:{{$teacher->mobile}}"><i class="icon-phone"></i></button>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>

    @empty($teachers->toArray())
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Teachers are empty, nothing to show</p>
            </div>
        </div>
    @endempty

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush