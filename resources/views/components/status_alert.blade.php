@if (session('status'))
    <div uk-alert class="alert">
        <a class="uk-alert-close" uk-close></a>
        <p><i class="icon-info"></i> {{session('status')}}</p>
    </div>
@endif