@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/chat.css')}}" />
@endpush

@section('body')
	<div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('chat-list')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">{{$user->name}}</p>
            </div>
        </div>
    </div>

    <div class="uk-padding-small uk-width-1-1">
		<div class="chat-section">
			<!-- Chats go here -->
		</div>

        @include('components.status_alert')

        @if (!$user->is_verified)
            <div class="chat-approve">
                <hr class="uk-divider-icon">
                <h3>This user is not approved</h3>
                <p>Approv this user by click the btn</p>
                <button href="{{route('chat-approve', ['user_id' => $user->id])}}" class="btn-primary uk-width-auto"><i class="icon-check"></i> Approve</button>
            </div>
        @endif

		<div class="chat-type-section uk-box-shadow-medium uk-width-1-1">
			<div uk-grid class="uk-grid-small">
				<div class="uk-flex uk-flex-middle uk-width-auto">
					<button class="chat-type-button"><i class="icon-smile"></i></button>
				</div>
				<div class="uk-flex uk-flex-middle uk-width-expand">
					<input id="message" type="text" placeholder="Type here" class="chat-type">
				</div>
				<div class="uk-flex uk-flex-middle uk-width-auto">
					<button onclick="sent()" class="chat-type-button chat-sent-button uk-box-shadow-large"><i class="icon-corner-down-right"></i></button>
				</div>
			</div>
		</div>
    </div>
@endsection

@push('script')
<script type="text/javascript">
	var last_message_id = -1;

	$(function() {
		get();
		setInterval("get()", 1000);
	});

	function get(){
		var data = { 
			"_token": $('#csrf-token')[0].content,
            user_id: "{{$user->id}}"
		};
		$.post("{{route('chat-get')}}", data)
		.done(function(data) {
			$('.chat-head-placeholder').last().remove();
			data.chats.forEach(function (chat){
				if(last_message_id<chat.id){
					last_message_id = chat.id;
					chatHTML = createChatHead(chat.message, timeDifference(new Date(chat.created_at)), chat.type);
					$('.chat-section').append(chatHtml);
					$(document).scrollTop($(document).height());
				}
			});
		});
	}

	function sent(){
		var data = { 
			message: $("#message").val(),
            user_id: "{{$user->id}}",
			"_token": $('#csrf-token')[0].content
		};
		$("#message").val("")
		chatHTML = createChatHead($("#message").val(), 'Sending...', 0, true);
		$('.chat-section').append(chatHtml);
		$(document).scrollTop($(document).height());
		$.post("{{route('chat-sent', ['type' => 0])}}", data)
		.done(function( data ) {
			get();
		});
	}

	function createChatHead(message, date, type, isloading=false){
		chatHtml = `
			<div class="chat-head-${(type==1)?'outgoing':'incoming'} uk-box-shadow-medium ${(isloading)?'chat-head-placeholder':''}">
				<p class="chat-head-msg">${message}</p>
				<p class="chat-head-time">${date}</p>
			</div>
		`;
		return chatHtml;
	}

	function timeDifference(previous) {
		var current = new Date();
	    var msPerMinute = 60 * 1000;
	    var msPerHour = msPerMinute * 60;
	    var msPerDay = msPerHour * 24;
	    var msPerMonth = msPerDay * 30;
	    var msPerYear = msPerDay * 365;

	    var elapsed = current - previous;

	    if (elapsed < msPerMinute) {
	         return Math.round(elapsed/1000) + ' sec ago';   
	    }

	    else if (elapsed < msPerHour) {
	         return Math.round(elapsed/msPerMinute) + ' min ago';   
	    }

	    else if (elapsed < msPerDay ) {
	         return Math.round(elapsed/msPerHour ) + ' hours ago';   
	    }

	    else if (elapsed < msPerMonth) {
	        return Math.round(elapsed/msPerDay) + ' days ago';   
	    }

	    else if (elapsed < msPerYear) {
	        return Math.round(elapsed/msPerMonth) + ' months ago';   
	    }

	    else {
	        return Math.round(elapsed/msPerYear ) + ' years ago';   
	    }
	}
</script>
@endpush