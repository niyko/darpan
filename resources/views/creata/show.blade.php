@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/creata.css')}}?integrity={{integrity('css/creata.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('more-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">CREATA</p>
            </div>

            @if (Auth::user()->is_super_admin)
                <div class="uk-width-auto">
                    <button href="#modal-sort" uk-toggle class="nav_sub_icon_btn uk-width-auto"><i class="icon-filter"></i> <span class="nav_sub_icon_btn_chip">{{$class_name}}</span></button>
                    
                    <div id="modal-sort" uk-modal>
                        <div class="uk-modal-dialog creata-drop">
                            <ul class="uk-list uk-list-divider uk-padding-remove">
                                @foreach ($classlist as $class)
                                    <li><a class="creata-drop-link uk-margin-medium-right" href="{{route('creata-show')}}?sort={{$class->id}}">{{$class->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
    
    <div class="uk-padding-small creata-list-padding-bottom-large">
        @include('components.status_alert')

        <div uk-grid="masonry: true">
            @foreach ($files as $file)
                @if ($file->user!=null)
                    <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
                        @php
                            $file_extension = explode(".", $file->url);
                            end($file_extension);
                            $file_extension = strtolower(current($file_extension));
                        @endphp
                        <div class="file-card uk-width-1-1 uk-box-shadow-large">
                            @if ($file_extension=='png' || $file_extension=='jpg' || $file_extension=='jpeg')
                                <img class="file-card-image uk-width-1-1" src="{{asset('uploads/creata/'.$file->url)}}">
                            @elseif ($file_extension=='mp4' || $file_extension=='m4v')
                                <img class="uk-width-1-1" src="{{asset('images/video_placeholder.png')}}">
                            @else
                                <img class="uk-width-1-1" src="{{asset('images/doc_placeholder.png')}}">
                            @endif
                            <div class="uk-padding-small">
                                <p class="file-card-title">{{$file->title}}</p>
                                <p class="file-card-description">{{$file->description}}</p>
                                @if ($file_extension=='mp4' || $file_extension=='m4v')
                                    <a class="file-card-link" href="{{asset('uploads/creata/'.$file->url)}}?open_in_video"><i class="icon-eye"></i> View</a>
                                @elseif ($file_extension=='png' || $file_extension=='jpg' || $file_extension=='jpeg')
                                    <a class="file-card-link" href="{{asset('uploads/creata/'.$file->url)}}?open_in_image"><i class="icon-eye"></i> View</a>
                                @else
                                    <a class="file-card-link" href="{{$file->url}}?open_in_url"><i class="icon-eye"></i> View</a>
                                @endif
                                @if ($file->user_id==Auth::id() || Auth::user()->is_admin)
                                    <a class="file-card-link" href="{{route('creata-delete', ['id' => $file->id])}}"><i class="icon-trash-2"></i> Delete</a>
                                @endif
                                @if (Auth::user()->is_admin)
                                    <a class="file-card-link" href="{{asset('uploads/creata/'.$file->url)}}?open_in_download"><i class="icon-download"></i> Download</a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        {{ $files->links() }}

        @if (!Auth::user()->is_admin)
        <div class="floating-btn-conatiner uk-width-1-1 uk-flex uk-flex-center">
            <button href="{{route('creata-create')}}" class="floating-btn uk-box-shadow-medium"><i class="icon-plus"></i> Upload</button>
        </div>
        @endif
        
    </div>

    @unless (count($files))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Files are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush