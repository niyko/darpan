@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/creata.css')}}?integrity={{integrity('css/creata.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('creata-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Upload</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')

        <form action="{{route('creata-store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <label>Upload title</label>
            <input name="title" autocomplete="off" required maxlength="50" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">

            <label>Upload description</label>
            <input name="description" autocomplete="off" required maxlength="200" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">

            <input autocomplete="off" type="hidden" name="type" value="0">

            <div uk-grid class="uk-grid-collapse uk-margin-small-top uk-margin-small-bottom">
                <div class="uk-width-auto">
                    <button type="button" onclick="changeUploadSlide(0,this)" class="toggle-button active">Image</button>
                </div>
                <div class="uk-width-auto">
                    <button type="button" onclick="changeUploadSlide(1,this)" class="toggle-button">Video</button>
                </div>
                <div class="uk-width-auto">
                    <button type="button" onclick="changeUploadSlide(2,this)" class="toggle-button">Link</button>
                </div>
            </div>

            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow id="myclassroom-upload-slider">
                <ul class="uk-slideshow-items">
                    <li>
                        <label>Upload image</label>
                        <div uk-form-custom="target: true" class="form-input uk-width-1-1">
                            <input autocomplete="off" name="upload_file" type="file" accept="image/*">
                            <input class="uk-width-1-1 uk-form-width-medium form-upload" type="text" placeholder="Select file" disabled>
                        </div>
                    </li>
                    <li>
                        <label>Upload video</label>
                        <div uk-form-custom="target: true" class="form-input uk-width-1-1">
                            <input autocomplete="off" name="upload_file" type="file" accept="video/*">
                            <input class="uk-width-1-1 uk-form-width-medium form-upload" type="text" placeholder="Select file" disabled>
                        </div>
                    </li>
                    <li>
                        <label>Upload link</label>
                        <input autocomplete="off" name="link" maxlength="300" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">
                    </li>
                </ul>
            </div>

            <button class="uk-width-1-1 btn-primary" type="submit"><i class="icon-upload"></i> Upload</button>
        </form>
    </div>
    
@endsection

@push('script')
<script src="{{asset('js/jquery.form.js')}}"></script> 
<script type="text/javascript">
	$('form').ajaxForm({
        beforeSend: function() {
            console.log(`--show-loading`);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            console.log(`--set-loading::${percentComplete}`);
        },
        complete: function(xhr) {
            window.location = `{{route('creata-show')}}`;
        }
    });

    //document.getElementById("file").addEventListener("change", function (event) {
	//    compress(event);
    //});
    //
    //function compress(e) {
    //    const width = 500;
    //    const height = 300;
    //    const fileName = e.target.files[0].name;
    //    const reader = new FileReader();
    //    reader.readAsDataURL(e.target.files[0]);
    //    reader.onload = event => {
    //        const img = new Image();
    //        img.src = event.target.result;
    //        img.onload = () => {
    //                elem = document.createElement('canvas');
    //                elem.width = width;
    //                elem.height = height;
    //                ctx = elem.getContext('2d');
    //                ctx.drawImage(img, 0, 0, width, height);
    //            },
    //            reader.onerror = error => console.log(error);
    //    };
    //}
//
    //function upload(){
    //    console.log(`--show-loading`);
    //    $.ajax({
    //        type: "POST",
    //        url: "{{route('creata-store')}}",
    //        data: { 
    //            "_token": "{{ csrf_token() }}",
    //            title: $('[name=title]').val(),
    //            description: $('[name=description]').val(),
    //            link: $('[name=link]').val(),
    //            media: (typeof elem !== 'undefined')?elem.toDataURL():""
    //        }
    //    }).done(function(o) {
    //        window.location = "{{route('creata-show')}}";
    //    });
    //}

    function changeUploadSlide(index,e){
        $('[name=type]').val(index);
        $('.toggle-button').removeClass('active');
        $(e).addClass('active');
        UIkit.slideshow('#myclassroom-upload-slider').show(index);
    }
</script>
@endpush