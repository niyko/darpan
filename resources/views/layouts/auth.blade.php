@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/auth.css')}}" />
@endpush

@section('body')
    <div class="auth-container uk-width-1-1 uk-flex uk-flex-center" style="background-image: url('{{asset('images/more_bg.png')}}');">
        <div class="auth-card-container">
            <p class="auth-logo-top">Welcome to</p>
            <h3 class="auth-logo-text">{{$school_code}}</h3>
            <span class="auth-menu"><a href="login">Login</a><a href="register">Register</a></span>
            <div class="auth-card uk-box-shadow-large">
                @yield('form')
            </div>
        </div>
    </div>
@endsection