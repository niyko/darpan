@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/quiz.css')}}?integrity={{integrity('css/quiz.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('more-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Quiz</p>
            </div>
            @if (Auth::user()->is_admin)
                <div class="uk-width-auto">
                    <button href="{{route('quiz-create')}}" class="nav_sub_icon_btn"><i class="icon-plus"></i></button>
                </div>
            @endif
            @if (Auth::user()->is_super_admin)
                <div class="uk-width-auto">
                    <button href="#modal-example" uk-toggle class="nav_sub_icon_btn uk-width-auto"><i class="icon-filter"></i> <span class="nav_sub_icon_btn_chip">{{$class_name}}</span></button>
                    
                    <div id="modal-example" uk-modal>
                        <div class="uk-modal-dialog quiz-drop">
                            <ul class="uk-list uk-list-divider uk-padding-remove">
                                @foreach ($classlist as $class)
                                    <li><a class="quiz-drop-link uk-margin-medium-right" href="{{route('quiz-show')}}?sort={{$class->id}}">{{$class->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
    
    <div class="uk-padding-small creata-list-padding-bottom-large">
        @include('components.status_alert')

        <div uk-grid="masonry: true">
            @foreach ($exams as $exam)
                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m uk-first-column">
                    <div class="file-card uk-width-1-1 uk-box-shadow-large">
                        <div uk-grid="" class="uk-grid-collapse uk-grid">
                            <div class="uk-width-auto uk-first-column" style="padding-bottom: 20px;">
                                @if (count($answers->where('exam_id', $exam->id))<=0)
                                    <button class="file-card-link-icon" style="background-image: url('{{asset('images/quiz_placeholder.png')}}')"><i class="icon-file-text"></i></button>
                                @else
                                    <button class="file-card-link-icon-text" style="background-image: url('{{asset('images/quiz_placeholder.png')}}')">{{$answers->where('exam_id', $exam->id)->first()->score}}/{{$exam->total_score}}</button>
                                @endif
                            </div>
                            <div class="uk-width-expand">
                                <div class="uk-padding-small">
                                    <p class="file-card-title">{{$exam->title}}</p>
                                    <p class="file-card-description">{{$exam->description}}</p>
                                    @if (!Auth::user()->is_admin)
                                        @if (count($answers->where('exam_id', $exam->id))<=0)
                                            <p class="file-card-username"><i class="icon-award"></i> Not attended</p>
                                        @else
                                            <p class="file-card-username"><i class="icon-clock"></i> {{\Carbon\Carbon::parse($answers->where('exam_id', $exam->id)->first()->created_at)->format('d M Y')}}</p>
                                        @endif
                                    @endif
                                    @if (Auth::user()->is_admin)
                                        @if (Auth::user()->is_super_admin)
                                            <span class="uk-label chip">{{$exam->classroom->name}}</span>
                                        @endif
                                        <span class="uk-label chip">{{($exam->is_published)?'Published':'Unpublished'}}</span><br>
                                        <a class="file-card-link" href="{{route('quiz-analyse', ['exam_id' => $exam->id])}}"><i class="icon-bar-chart-2"></i> Analyse</a>
                                        @if ($exam->is_published)
                                            <a class="file-card-link" href="{{route('quiz-questions', ['exam_id' => $exam->id])}}"><i class="icon-eye"></i> View</a>
                                        @else
                                            <a class="file-card-link" href="{{route('quiz-questions', ['exam_id' => $exam->id])}}"><i class="icon-edit"></i> Edit</a>
                                        @endif
                                        <a class="file-card-link" href="{{route('quiz-delete', ['id' => $exam->id])}}"><i class="icon-trash-2"></i> Delete</a>
                                    @else
                                        @if (count($answers->where('exam_id', $exam->id))<=0)
                                            <a class="file-card-link" href="{{route('quiz-exam', ['exam_id' => $exam->id])}}"><i class="icon-edit-2"></i> Attend</a>
                                        @else
                                            <a class="file-card-link" href="{{route('quiz-view-answers', ['exam_id' => $exam->id, 'user_id' => Auth::id()])}}"><i class="icon-eye"></i> View your Answers</a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        {{ $exams->links() }}

    </div>

    @unless (count($exams))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Quiz are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush