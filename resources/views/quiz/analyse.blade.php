@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/quiz.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('quiz-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">{{$exam->title}}</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small creata-list-padding-bottom-large">
        @include('components.status_alert')

        <ul class="uk-list uk-list-divider list">
            @foreach ($results as $result)
                <li>
                    <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
                        <div class="uk-width-auto">
                            <button class="quiz-analyse-list-score">{{$result->score}}</button>
                        </div>
                        <div class="uk-width-expand name">
                            {{$result->user->name}}
                        </div>
                        <div class="uk-width-auto">
                            <button class="btn-primary btn-small" href="{{route('quiz-view-answers', ['user_id' => $result->user_id, 'exam_id' => $exam->id])}}"><i class="icon-bar-chart-2"></i></button>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>

    </div>

    @unless (count($results))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Results are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush