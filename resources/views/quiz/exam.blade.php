@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/quiz.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('quiz-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">{{$exam->title}}</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small creata-list-padding-bottom-large">
        @include('components.status_alert')

        <form action="{{route('quiz-exam-store', ['exam_id' => $exam->id])}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div>
                @php
                    $i = 1;
                @endphp
                @foreach ($questions as $question)
                    <h3 class="quiz-exam-questions"><button>{{$i++}}</button> {{$question->question}}</h3>
                    <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                        @foreach ($question->options as $option)
                            <div class="quiz-exam-option uk-width-1-1">
                                <div uk-grid class="uk-grid-collapse">
                                    <div class="uk-width-auto"><input class="uk-radio" type="radio" value="{{($option->is_correct)?'true':'false'}}::{{$option->id}}" {{($option->is_correct)?'required':''}} name="answers[{{$question->id}}]"></div>
                                    <div class="uk-width-expand"><label>{{$option->option}}</label></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <hr>
                @endforeach

                <button class="uk-width-1-1 btn-primary" type="submit"><i class="icon-check"></i> Done</button>
            </div>
        </form>

    </div>

    @unless (count($questions))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Questions are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush