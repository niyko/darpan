@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/quiz.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('quiz-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Create exam</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')
        <form action="{{route('quiz-store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <label>Exam title</label>
            <input name="title" required maxlength="50" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">

            <label>Exam description</label>
            <input name="description" required maxlength="200" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">

            <label>Class</label>
            <div class="form-select">
                <select name="class_id" required class="uk-select">
                    @if (Auth::user()->is_super_admin)
                        <option selected disabled>Select one</option>
                        @foreach ($classlist as $class)
                            <option value="{{$class->id}}">{{$class->name}}</option>
                        @endforeach
                    @else
                        <option selected value="{{Auth::user()->class_id}}">{{Auth::user()->classroom->name}}</option>
                    @endif
                </select>
            </div>

            <button class="uk-width-1-1 btn-primary" type="submit"><i class="icon-plus"></i> Add</button>
        </form>
    </div>
    
@endsection

@push('script')
<script type="text/javascript">

</script>
@endpush