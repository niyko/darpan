@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/quiz.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('quiz-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">{{$exam->title}}</p>
            </div>
            @if (!$exam->is_published)
                <div class="uk-width-auto">
                    <button href="{{route('quiz-question-create', ['exam_id' => $exam->id])}}" class="nav_sub_icon_btn"><i class="icon-plus"></i></button>
                </div>
            @endif
        </div>
    </div>
    
    <div class="uk-padding-small creata-list-padding-bottom-large">
        @include('components.status_alert')

        <div uk-grid="masonry: true">
            @foreach ($questions as $question)
                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m uk-first-column">
                    <div class="file-card uk-width-1-1 uk-box-shadow-large">
                        <div class="uk-padding-small">
                            <p class="file-card-title">{{$question->question}}</p>
                            @if (!$exam->is_published)
                                <a class="file-card-link" href="{{route('quiz-options', ['question_id' => $question->id])}}"><i class="icon-edit"></i> Edit</a>
                            @else
                                <a class="file-card-link" href="{{route('quiz-options', ['question_id' => $question->id])}}"><i class="icon-eye"></i> View</a>
                            @endif
                            @if (!$exam->is_published)
                                <a class="file-card-link" href="{{route('quiz-question-delete', ['question_id' => $question->id])}}"><i class="icon-trash-2"></i> Delete</a>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        {{ $questions->links() }}

        @if (!$exam->is_published)
            <button href="{{route('quiz-publish', ['exam_id' => $exam->id])}}" class="uk-width-1-1 btn-primary uk-margin-large-top"><i class="icon-check"></i> Publish</button>
            <p class="small-btn-msg"><i class="icon-info"></i> Please note, once published you can't add or edit to this quiz</p>
        @endif

    </div>

    @unless (count($questions))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Questions are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush