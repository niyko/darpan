@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/quiz.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('quiz-options', ['question_id' => $question->id])}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Create option</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')
        <form action="{{route('quiz-option-store', ['question_id' => $question->id])}}" method="POST" enctype="multipart/form-data">
            @csrf
            <label>Option</label>
            <textarea name="option" required maxlength="1000" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text"></textarea>

            <label><input class="uk-checkbox" name="is_correct" value="1" type="checkbox"> Correct answer</label>

            <button class="uk-width-1-1 btn-primary" type="submit"><i class="icon-plus"></i> Add</button>
        </form>
    </div>
    
@endsection

@push('script')
<script type="text/javascript">

</script>
@endpush