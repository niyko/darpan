@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/quiz.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('quiz-questions', ['exam_id' => $question->exam_id])}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">{{$question->question}}</p>
            </div>
            @if (!$question->exam->is_published)
                <div class="uk-width-auto">
                    <button href="{{route('quiz-option-create', ['question_id' => $question->id])}}" class="nav_sub_icon_btn"><i class="icon-plus"></i></button>
                </div>
            @endif
        </div>
    </div>
    
    <div class="uk-padding-small creata-list-padding-bottom-large">
        @include('components.status_alert')

        <ul class="uk-list uk-list-divider list">
            @foreach ($options as $option)
                <li>
                    <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
                        <div class="uk-width-expand name">
                            {{$option->option}}
                            @if ($option->is_correct)
                                <span class="uk-label list-chip">Correct</span>
                            @endif
                        </div>
                        @if (!$question->exam->is_published)
                            <div class="uk-width-auto">
                                <button class="btn-primary btn-small" href="{{route('quiz-option-delete', ['option_id' => $option->id])}}"><i class="icon-trash-2"></i></button>
                            </div>
                        @endif
                    </div>
                </li>
            @endforeach
        </ul>

        {{ $options->links() }}

    </div>

    @unless (count($options))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Options are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush