@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/quiz.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{ url()->previous() }}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">{{$exam->title}}</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small creata-list-padding-bottom-large">
        @include('components.status_alert')

        <form>
            @csrf
            <div>
                @php
                    $i = 1;
                @endphp
                @foreach ($questions as $question)
                    <h3 class="quiz-exam-questions"><button>{{$i++}}</button> {{$question->question}}</h3>
                    <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                        @foreach ($question->options as $option)
                            <div class="quiz-exam-option {{($option->is_correct)?'correct':''}} uk-width-1-1">
                                <div uk-grid class="uk-grid-collapse">
                                    @if (count($answers->where('option_id', $option->id))<=0)
                                        <div class="uk-width-auto"><input class="uk-radio" type="radio" disabled></div>
                                    @else
                                        <div class="uk-width-auto"><input class="uk-radio" type="radio" checked></div>
                                    @endif
                                    <div class="uk-width-expand"><label>{{$option->option}}</label></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <hr>
                @endforeach
            </div>
        </form>

    </div>

    @unless (count($questions))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Answers are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush