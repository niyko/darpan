@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/creata.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('noticeboard-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Sent notice</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')

        <form action="{{route('noticeboard-store')}}" method="POST" enctype="multipart/form-data">
            @csrf

            <label>Class</label>
            <div class="form-select">
                <select name="class_id" required class="uk-select">
                    <option selected disabled>Select one</option>
                    @if (Auth::user()->is_super_admin)
                        <option value="-1">All</option>
                        @foreach ($classlist as $class)
                            <option value="{{$class->id}}">{{$class->name}}</option>
                        @endforeach
                    @else
                        <option value="{{Auth::user()->class_id}}">Your class</option>
                    @endif
                </select>
            </div>

            <label>Notice</label>
            <textarea name="notice" required maxlength="1000" style="height: 227px;" class="form-input uk-width-1-1 uk-margin-small-bottom" placeholder="Write here"></textarea>

            <button class="uk-width-1-1 btn-primary" type="submit"><i class="icon-send"></i> Send</button>
        </form>
    </div>
    
@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush