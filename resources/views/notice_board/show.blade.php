@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/notice_board.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('more-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Notice board</p>
            </div>
            @if (Auth::user()->is_admin)
                <div class="uk-width-auto">
                    <button href="{{route('noticeboard-create')}}" class="nav_sub_icon_btn"><i class="icon-plus"></i></button>
                </div>
            @endif
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')

        <div uk-grid="masonry: true">
            @foreach ($noticebaord as $notice)
                @if ($notice->user!=null)
                    <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
                        <div class="notice-board-card">
                            <p>{!!$notice->notice!!}</p>
                            @if ($notice->user->is_admin)
                                <div uk-grid class="uk-grid-small">
                                    <div class="uk-width-expand">
                                        <span class="uk-label notice-board-card-label">By {{$notice->user->name}}</span>
                                        <span class="uk-label notice-board-card-label">{{($notice->class_id==-1)?'To all class':$notice->classroom->name}}</span>
                                        <span class="uk-label notice-board-card-label">{{Carbon\Carbon::parse($notice->created_at)->diffForHumans()}}</span>
                                    </div>
                                    <div class="uk-width-auto uk-flex uk-flex-bottom " style="display: none;">
                                        <button href="{{route('noticeboard-delete', ['id' => $notice->id])}}" class="btn-primary btn-small">Delete</button>
                                    </div>
                                </div>
                            @else
                                <div uk-grid class="uk-grid-small uk-margin-small-top">
                                    <div class="uk-width-auto">
                                        @if ($notice->user->is_super_admin)
                                            <p class="notice-board-card-user-badge">By School management</p>
                                        @else
                                            <span class="notice-board-card-time">By {{$notice->user->name}}</span>
                                        @endif
                                    </div>
                                    <div class="uk-width-expand">
                                        <span class="notice-board-card-time">{{Carbon\Carbon::parse($notice->created_at)->diffForHumans()}}</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        {{ $noticebaord->links() }}

    </div>

    @unless (count($noticebaord))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Notice are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush