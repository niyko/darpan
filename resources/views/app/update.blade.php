@extends('app')

@push('head')

@endpush

@section('body')
    <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
        <div class="uk-margin-medium-top">
            <i class="icon-download"></i>
            <h3>Update the App</h3>
            <p>Update the app to continue</p>
            <button class="btn-primary" href="https://play.google.com/store/apps/details?id=com.creatyification.darpanapp"><i class="icon-arrow-down-circle"></i> Update</button>
        </div>
    </div>
@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush