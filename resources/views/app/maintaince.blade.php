@extends('app')

@push('head')

@endpush

@section('body')
    <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
        <div class="uk-margin-medium-top">
            <i class="icon-alert-triangle"></i>
            <h3>App is under maintenance</h3>
            <p ondblclick="window.location='/';">This will take probably under 2-3 hrs</p>
        </div>
    </div>
@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush