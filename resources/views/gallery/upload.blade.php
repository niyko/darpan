@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/gallery.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('gallery-gallery', ['album_id' => $album->id])}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Upload</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')

        <form action="{{route('gallery-gallery-store', ['album_id' => $album->id])}}" method="POST" enctype="multipart/form-data">
            @csrf
            <label>Upload title</label>
            <input name="title" required maxlength="50" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">

            <label>Upload file</label>
            <div uk-form-custom="target: true" class="form-input uk-width-1-1">
                <input name="media" required type="file" accept="image/*">
                <input class="uk-width-1-1 uk-form-width-medium form-upload" type="text" placeholder="Select file" disabled>
            </div>

            <button class="uk-width-1-1 btn-primary" type="submit"><i class="icon-upload"></i> Upload</button>
        </form>
    </div>
    
@endsection

@push('script')
<script src="{{asset('js/jquery.form.js')}}"></script> 
<script type="text/javascript">
    $('form').ajaxForm({
        beforeSend: function() {
            console.log(`--show-loading`);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            console.log(`--set-loading::${percentComplete}`);
        },
        complete: function(xhr) {
            window.location = `{{route('gallery-gallery', ['album_id' => $album->id])}}`;
        }
    });
</script>
@endpush