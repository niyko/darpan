@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/gallery.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('gallery-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">{{$album->title}}</p>
            </div>
            @if (Auth::user()->is_super_admin)
                <div class="uk-width-auto">
                    <button href="{{route('gallery-gallery-create', ['album_id' => $album->id])}}" class="nav_sub_icon_btn"><i class="icon-upload"></i></button>
                </div>
                <div class="uk-width-auto">
                    <button href="{{route('gallery-album-delete', ['album_id' => $album->id])}}" class="nav_sub_icon_btn"><i class="icon-trash-2"></i></button>
                </div>
            @endif
        </div>
    </div>
    
    <div class="">
        @include('components.status_alert')

        <div uk-lightbox="animation: fade">
            <div uk-grid="masonry: true" class="uk-grid-collapse">
                @foreach($pictures as $picture)
                    <div class="uk-width-1-2 uk-width-1-4@m uk-width-1-6@l">
                        <div class="gallery-image-outer uk-width-1-1">
                            <a href="{{asset('uploads/gallery/'.$picture->url)}}" data-caption="{{$picture->title}}">
                                <img class="uk-width-1-1" src="{{asset('uploads/gallery_thumbs/'.$picture->url)}}">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        {{ $pictures->links() }}

    </div>

    @unless (count($pictures))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Images are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush