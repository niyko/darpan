@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/gallery.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('more-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Gallery</p>
            </div>
            @if (Auth::user()->is_super_admin)
                <div class="uk-width-auto">
                    <button href="{{route('gallery-album-create')}}" class="nav_sub_icon_btn"><i class="icon-plus"></i></button>
                </div>
            @endif
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')

        <div uk-grid="masonry: true">
            @foreach ($albums as $album)
                <div class="uk-width-1-2 uk-width-1-4@m uk-width-1-6@l">
                    @if (count($album->media)==0)
                    <div href="{{route('gallery-gallery', ['album_id' => $album->id])}}" class="gallery-folder-card uk-width-1-1 uk-box-shadow-large" style="background-image: url('https://picsum.photos/200/300?rand={{rand()}}');">
                    @else
                    <div href="{{route('gallery-gallery', ['album_id' => $album->id])}}" class="gallery-folder-card uk-width-1-1 uk-box-shadow-large" style="background-image: url('{{asset('uploads/gallery_thumbs/'.$album->media[0]->url)}}');">
                    @endif
                        <div class="gallery-folder-card-inner uk-width-1-1 uk-flex uk-flex-bottom">
                            <p class="gallery-folder-title">{{$album->title}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        {{ $albums->links() }}

    </div>

    @unless (count($albums))
        <div class="uk-padding-small empty-list uk-flex uk-flex-middle uk-flex-center">
            <div>
                <i class="icon-layers"></i>
                <h3>Nothing found</h3>
                <p>Albums are empty, nothing to show</p>
            </div>
        </div>
    @endunless

@endsection

@push('script')
<script type="text/javascript">
	
</script>
@endpush