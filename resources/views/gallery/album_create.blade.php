@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/gallery.css')}}" />
@endpush

@section('body')
    <div class="nav_sub uk-width-1-1 uk-box-shadow-medium" style="z-index: 980;" uk-sticky="bottom: #offset">
        <div uk-grid class="uk-flex uk-flex-middle uk-grid-small">
            <div class="uk-width-auto">
                <button href="{{route('gallery-show')}}" class="nav_sub_icon_btn"><i class="icon-arrow-left"></i></button>
            </div>
            <div class="uk-width-expand">
                <p class="nav_sub_title">Create album</p>
            </div>
        </div>
    </div>
    
    <div class="uk-padding-small">
        @include('components.status_alert')

        <form action="{{route('gallery-album-store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <label>Album title</label>
            <input name="title" required maxlength="50" class="form-input uk-width-1-1 uk-margin-small-bottom" type="text" placeholder="Write here">

            <button class="uk-width-1-1 btn-primary" type="submit"><i class="icon-plus"></i> Create</button>
        </form>
    </div>
    
@endsection

@push('script')
<script src="http://malsup.github.com/jquery.form.js"></script> 
<script type="text/javascript">

</script>
@endpush