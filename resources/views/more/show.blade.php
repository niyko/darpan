@extends('app')

@push('head')
<link rel="stylesheet" href="{{asset('css/more.css')}}" />
@endpush

@section('body')
    <div class="uk-width-1-1 uk-overflow-hidden">
		<div class="more-profile uk-flex uk-flex-middle" style="background-image: url('{{asset('images/more_bg.png')}}');">
			<div>
				<p>Welcome back</p>
				<h3>{{Auth::user()->name}}</h3>
				<button onclick="console.log('--lougout');" href="{{route('signout')}}" class="btn-primary btn-small uk-width-auto"><i class="icon-lock"></i> Signout</button>
			</div>
		</div>
		<div class="more-conatiner uk-padding-small">
			<div uk-grid class="uk-child-width-1-3 uk-grid-small">
				<div>
					<div href="{{(Auth::user()->is_admin)?route('chat-list'):route('chat-show')}}" class="more-card uk-padding-small uk-box-shadow-medium">
						<img src="{{asset('images/chat_more.png')}}">
						<p>Chats</p>
					</div>
				</div>

				@if (Auth::user()->is_verified)
					<div>
						<div href="{{route('myclassroom-show')}}" class="more-card uk-padding-small uk-box-shadow-medium">
							<img src="{{asset('images/myclassroom_more.png')}}">
							<p>Classroom</p>
						</div>
					</div>

					<div>
						<div href="{{route('meetteachers-show')}}" class="more-card uk-padding-small uk-box-shadow-medium">
							<img src="{{asset('images/meetteachers_more.png')}}">
							<p>My Teachers</p>
						</div>
					</div>

					<div>
						<div href="{{route('noticeboard-show')}}" class="more-card uk-padding-small uk-box-shadow-medium">
							<img src="{{asset('images/noticeboard_more.png')}}">
							<p>Notice board</p>
						</div>
					</div>

					<div>
						<div href="{{route('quiz-show')}}" class="more-card uk-padding-small uk-box-shadow-medium">
							<img src="{{asset('images/quiz_more.png')}}">
							<p>Quiz</p>
						</div>
					</div>

					<div>
						<div href="{{route('bookshelf-show')}}" class="more-card uk-padding-small uk-box-shadow-medium">
							<img src="{{asset('images/bookshelf_more.png')}}">
							<p>Book shelf</p>
						</div>
					</div>

					<div>
						<div onclick="redirectTo('{{route('creata-show')}}');" class="more-card uk-padding-small uk-box-shadow-medium">
							<img src="{{asset('images/creata_more.png')}}">
							<p>Creata</p>
						</div>
					</div>

					<div>
						<div onclick="redirectTo('{{route('gallery-show')}}');" class="more-card uk-padding-small uk-box-shadow-medium">
							<img src="{{asset('images/gallery_more.png')}}">
							<p>Gallery</p>
						</div>
					</div>

					<div>
						<div href="https://bit.ly/3hnJ6Z1?open_in_url&title=Locate_bus" class="more-card uk-padding-small uk-box-shadow-medium">
							<img src="{{asset('images/bus_more.png')}}">
							<p>Locate bus</p>
						</div>
					</div>

					@if ($school_id=="PBKV")
						<div>
							<div href="https://www.youtube.com/channel/UCUI8TvIvwRuTq6SkpTEBFzA?open_in_url&title=Youtube" class="more-card uk-padding-small uk-box-shadow-medium">
								<img src="{{asset('images/youtube_more.png')}}">
								<p>Youtube</p>
							</div>
						</div>

						<div>
							<div href="https://www.facebook.com/Parama-Bhattara-Kendriya-Vidyalayam-459278384580163?open_in_url&title=Facebook" class="more-card uk-padding-small uk-box-shadow-medium">
								<img src="{{asset('images/facebook_more.png')}}">
								<p>Facebook</p>
							</div>
						</div>
					@endif

					<div>
					{{-- /vsk-mjty-hba --}}
						<div href="https://whiteboard.fi/?open_in_url&title=Connect_Me" class="more-card uk-padding-small uk-box-shadow-medium">
							<img src="{{asset('images/connectme_more.png')}}">
							<p>Connect me<br>(whiteboard)</p>
						</div>
					</div>

					<div>
					{{-- /vsk-mjty-hba --}}
						<div href="https://meet.jit.si/?open_in_url&title=Connect_Me" class="more-card uk-padding-small uk-box-shadow-medium">
							<img src="{{asset('images/connectme_more.png')}}">
							<p>Connect me<br>(jitsi)</p>
						</div>
					</div>

					@if (Auth::user()->is_super_admin)
						<div>
							<div href="{{route('users-show', ['class_id' => Auth::user()->class_id])}}" class="more-card uk-padding-small uk-box-shadow-medium">
								<img src="{{asset('images/users_more.png')}}">
								<p>Users</p>
							</div>
						</div>
					@endif

				@endif
			</div>
		</div>
    </div>
@endsection

@push('script')
<script type="text/javascript">

	$(function() {
		console.log('--clear-history');
		setNotificationIds();
	});

	function setNotificationIds(){
		console.log('--render-cmd-set_notify_id::{{$school_notification_id}}::{{$class_notification_id}}::{{$user_notification_id}}::{{$type_notification_id}}');
	}
</script>
@endpush
