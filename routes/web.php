<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group whicxc x x hcxcx
| contains the "web" middleware group. Now create something great!bdbdbd
|
*/

// URL::forceSchema('https');

Auth::routes();

Route::get('/signout', 'AuthController@logout')->name('signout');
Route::get('/open-app', 'AuthController@openApp')->name('open-app');
Route::get('/maintaince', function () {
    return view('app.maintaince');
});

//===============================================================================================================================================================>

Route::get('/chat', 'ChatController@show')->name('chat-show')->middleware('auth');
Route::post('/chat', 'ChatController@get')->name('chat-get')->middleware('auth');
Route::post('/chat/sent/{type}', 'ChatController@sent')->name('chat-sent')->middleware('auth');
Route::get('/chat/list', 'ChatController@list')->name('chat-list')->middleware('auth');
Route::get('/chat/admin/{user_id}', 'ChatController@admin')->name('chat-admin')->middleware('auth');
Route::get('/chat/approve/{user_id}', 'ChatController@approve')->name('chat-approve')->middleware('auth');
Route::post('/chat/attachment', 'ChatController@uploadAttachment')->name('chat-upload-attachment')->middleware('auth');

//===============================================================================================================================================================>

Route::get('/', 'MoreController@show')->name('more-show')->middleware('auth');

//===============================================================================================================================================================>

Route::get('/my-classroom', 'MyClassroomController@show')->name('myclassroom-show')->middleware('auth');
Route::get('/my-classroom/upload', 'MyClassroomController@create')->name('myclassroom-create')->middleware('auth');
Route::post('/my-classroom/upload', 'MyClassroomController@store')->name('myclassroom-store')->middleware('auth');
Route::get('/my-classroom/delete/{id}', 'MyClassroomController@delete')->name('myclassroom-delete')->middleware('auth');

//===============================================================================================================================================================>

Route::get('/meet-teachers', 'MeetTeachersController@show')->name('meetteachers-show')->middleware('auth');

//===============================================================================================================================================================>

Route::get('/users/{class_id}', 'UsersController@show')->name('users-show')->middleware('auth');
Route::get('/users/users-change-type/{user_id}/{type}', 'UsersController@changeType')->name('users-changetype')->middleware('auth');
Route::get('/users/delete/{user_id}', 'UsersController@delete')->name('users-delete')->middleware('auth');
Route::get('/users/verifiy/{user_id}', 'UsersController@verifiy')->name('users-verifiy')->middleware('auth');

//===============================================================================================================================================================>

Route::get('/creata', 'CreataController@show')->name('creata-show')->middleware('auth');
Route::get('/creata/upload', 'CreataController@create')->name('creata-create')->middleware('auth');
Route::post('/creata/upload', 'CreataController@store')->name('creata-store')->middleware('auth');
Route::get('/creata/delete/{id}', 'CreataController@delete')->name('creata-delete')->middleware('auth');

//===============================================================================================================================================================>

Route::get('/book-shelf', 'BookshelfController@show')->name('bookshelf-show')->middleware('auth');
Route::get('/book-shelf/upload', 'BookshelfController@create')->name('bookshelf-create')->middleware('auth');
Route::post('/book-shelf/upload', 'BookshelfController@store')->name('bookshelf-store')->middleware('auth');
Route::get('/book-shelf/delete/{id}', 'BookshelfController@delete')->name('bookshelf-delete')->middleware('auth');

//===============================================================================================================================================================>

Route::get('/notice-board', 'NoticeBoardController@show')->name('noticeboard-show')->middleware('auth');
Route::get('/notice-board/create', 'NoticeBoardController@create')->name('noticeboard-create')->middleware('auth');
Route::post('/notice-board/store', 'NoticeBoardController@store')->name('noticeboard-store')->middleware('auth');
Route::get('/notice-board/delete/{id}', 'NoticeBoardController@delete')->name('noticeboard-delete')->middleware('auth');

//================================================================================================================================================================>

Route::get('/quiz', 'QuizController@show')->name('quiz-show')->middleware('auth');
Route::get('/quiz/upload', 'QuizController@create')->name('quiz-create')->middleware('auth');
Route::post('/quiz/upload', 'QuizController@store')->name('quiz-store')->middleware('auth');
Route::get('/quiz/delete/{id}', 'QuizController@delete')->name('quiz-delete')->middleware('auth');

Route::get('/quiz/questions/show/{exam_id}', 'QuizController@questions')->name('quiz-questions')->middleware('auth');
Route::get('/quiz/questions/upload/{exam_id}', 'QuizController@questions_create')->name('quiz-question-create')->middleware('auth');
Route::post('/quiz/questions/upload/{exam_id}', 'QuizController@questions_store')->name('quiz-question-store')->middleware('auth');
Route::get('/quiz/questions/delete/{question_id}', 'QuizController@questions_delete')->name('quiz-question-delete')->middleware('auth');

Route::get('/quiz/options/show/{question_id}', 'QuizController@options')->name('quiz-options')->middleware('auth');
Route::get('/quiz/options/upload/{question_id}', 'QuizController@options_create')->name('quiz-option-create')->middleware('auth');
Route::post('/quiz/options/upload/{question_id}', 'QuizController@options_store')->name('quiz-option-store')->middleware('auth');
Route::get('/quiz/options/delete/{option_id}', 'QuizController@options_delete')->name('quiz-option-delete')->middleware('auth');

Route::get('/quiz/exam/attend/{exam_id}', 'QuizController@exam')->name('quiz-exam')->middleware('auth');
Route::post('/quiz/exam/upload/{exam_id}', 'QuizController@exam_store')->name('quiz-exam-store')->middleware('auth');

Route::get('/quiz/analyse/all/{exam_id}', 'QuizController@analyse')->name('quiz-analyse')->middleware('auth');
Route::get('/quiz/analyse/one/{exam_id}/{user_id}', 'QuizController@view_answers')->name('quiz-view-answers')->middleware('auth');

Route::get('/quiz/publish/{exam_id}', 'QuizController@publish')->name('quiz-publish')->middleware('auth');

//===============================================================================================================================================================>

Route::get('/gallery', 'GalleryController@show')->name('gallery-show')->middleware('auth');
Route::get('/gallery/create', 'GalleryController@album_create')->name('gallery-album-create')->middleware('auth');
Route::post('/gallery/store', 'GalleryController@album_store')->name('gallery-album-store')->middleware('auth');
Route::get('/gallery/album/delete/{album_id}', 'GalleryController@album_delete')->name('gallery-album-delete')->middleware('auth');

Route::get('/gallery/album/{album_id}', 'GalleryController@gallery')->name('gallery-gallery')->middleware('auth');
Route::get('/gallery/upload/{album_id}', 'GalleryController@gallery_create')->name('gallery-gallery-create')->middleware('auth');
Route::post('/gallery/upload/{album_id}', 'GalleryController@gallery_store')->name('gallery-gallery-store')->middleware('auth');

//===============================================================================================================================================================>
